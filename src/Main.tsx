import React, { Component } from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom'

import LoginPage from './Login'
import RegisterPage from './Register'

const MainContext = React.createContext({})

function Main() {
  return (
    <MainContext.Provider value={{ }}>
      <Router>
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
      </Router>
    </MainContext.Provider>
  )
}

export default Main