import React from 'react'

import parse from '../shared/services/parse'

interface RegisterPageController {
  email: string,
  password: string,
  handleChangeEmail: (event: React.ChangeEvent<HTMLInputElement>) => void,
  handleChangePassword: (event: React.ChangeEvent<HTMLInputElement>) => void,
  handleSubmit: (event: React.ChangeEvent<HTMLFormElement>) => void
}

function useRegisterPage(props: any): RegisterPageController {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  function handleChangeEmail(event: React.ChangeEvent<HTMLInputElement>) {
    setEmail(event.currentTarget.value)
  }

  function handleChangePassword(event: React.ChangeEvent<HTMLInputElement>) {
    setPassword(event.currentTarget.value)
  }

  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    const user = new parse.User()
   
    try {
      const response = user.save({
        email: email,
        password: password
      });
      
      console.log(response)
    } catch (error) {
      console.error(error)
    }
  }


  return {
    email,
    password,
    handleChangeEmail,
    handleChangePassword,
    handleSubmit
  }
}

function RegisterPage(props: any) {
  const controller = useRegisterPage(props)

  return (
    <div>
      <form onSubmit={controller.handleSubmit}>
        <label>Email</label>
        <input 
          value={controller.email} 
          onChange={controller.handleChangeEmail}
        />
        <label>Password</label>
        <input 
          value={controller.password}
          onChange={controller.handleChangePassword}
        />
        <button>Register</button>
      </form>
    </div>
  )
}

export default RegisterPage